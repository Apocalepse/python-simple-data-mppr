# -*- coding: utf-8 -*-
"""
Библиотека реализует класс для маппинга данных в БД, предназначен в основном для записи данных

"""
import datetime


class Model:
    """
    Базовый класс модели данных, реализующий логику вставки и обновления данных

    """
    id = None

    @property
    def table_name(self):
        raise NotImplementedError

    @property
    def primary_key(self):
        raise NotImplementedError

    @property
    def fields(self):
        """
        Список полей модели, ключи должны совпадать с полямт в БД

        Returns:
            list: список полей модели

        """
        raise NotImplemented

    def build_insert_query(self, field_values):
        """

        Args:
            field_values (dict): значения, которые следует записать

        Returns:
            str: строка SQL запроса

        """
        field_values_expr = self._build_insert_expr(field_values)

        return 'INSERT INTO {} {}'.format(self.table_name, field_values_expr)

    def build_update_by_pk_query(self, field_values, pk):
        """
        Строит SQL запрос для обновления записи по первичному ключу

        Args:
            field_values (dict): field_values (dict): значения, которые следует обновить.
            pk (int): значение первичного ключа

        Returns:
            str: запрос для обновления

        """
        field_values_expr = self._build_update_expr(field_values)

        return 'UPDATE {} SET {} WHERE {}={}'.format(self.table_name, field_values_expr, self.primary_key, pk)

    def build_math_query(self, conditions):
        """
        Строит запрос для сопоставления по заданным полям

        Args:
            conditions (dict): значение полей, по которым производится поиск

        Returns:
            str: строку запроса SQL

        """
        where_expr = self._build_where_and_expr(conditions)

        return 'SELECT {} FROM {} WHERE {}'.format(self.primary_key, self.table_name, where_expr)

    def _validate_field_name(self, field_name):
        if field_name not in self.fields:
            raise KeyError("Ключ {} не описан в модели {}".format(field_name, self.__class__.__name__))

        return field_name

    def _prepare_field_value(self, field_value):
        """
        Подготавливает значение для подстановки в запрос SQL

        Args:
            field_value: значение поля для SQL запроса

        Returns:
            str: строка со значением для подстановки в SQL запрос

        """
        if field_value is None:
            return 'NULL'
        elif isinstance(field_value, bool):
            return 'TRUE' if field_value else 'FALSE'
        elif isinstance(field_value, datetime.datetime):
            if field_value.tzinfo is None:
                return "'{}'".format(field_value.strftime('%Y-%m-%d %H:%M:%S.%f'))
            else:
                return "'{}'".format(field_value.strftime('%Y-%m-%d %H:%M:%S.%f %z'))
        if isinstance(field_value, datetime.date):
            return "'{}'".format(field_value.strftime('%Y-%m-%d'))
        elif isinstance(field_value, datetime.time):
            return "'{}'".format(field_value.strftime('%H:%M:%S.%f'))
        elif isinstance(field_value, (int, float)):
            return str(field_value)
        else:
            # Любой объект, имеющий строковое представление, передается в параметры запроса в кавычках
            return "'{}'".format(field_value.replace("'", "''"))

    def _build_where_and_expr(self, conditions):
        """
        Строит SQL выражение для WHERE объединяя условия через AND

        Args:
            conditions (dict): условия в виде словаря где ключи совпадают с ключами self.fields
            а значения совпадают со значениями в БД

        Returns:
            str: выражение для WHERE

        """
        _where_expr = []

        for key, value in conditions.items():
            _where_expr.append('{}={}'.format(self._validate_field_name(key), self._prepare_field_value(value)))

        return ' AND '.join(_where_expr)

    def _build_insert_expr(self, field_values):
        _field_names = [self._validate_field_name(field) for field in field_values.keys()]
        _field_values = [self._prepare_field_value(value) for value in field_values.values()]

        return '({}) VALUES ({})'.format(
            ', '.join(_field_names),
            ', '.join(_field_values),
        )

    def _build_update_expr(self, field_values):
        """
        Строит часть SQL выражения для UPDATE

        Args:
            field_values (dict): значения, которые необходимо обновить

        Returns:
            str: часть SQL выражения для UPDATE

        """

        # сформируем лист готовых строк имя_поля=значение
        kv_parts = [
            '{}={}'.format(self._validate_field_name(k), self._prepare_field_value(v))
            for k, v in field_values.items()
        ]

        return ', '.join(kv_parts)
