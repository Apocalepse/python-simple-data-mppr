# -*- coding: utf-8 -*-
import os
import datetime
from unittest import TestCase

from . import models
from . import manager


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SQLITE_DB_PATH = os.path.join(BASE_DIR, 'test_db.sqlite3')


class TestModel(models.Model):
    primary_key = 'id'

    fields = [
        'title',
        'created_at',
    ]

    @property
    def table_name(self):
        return self.__class__.__name__


class ManagerTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.manager = manager.Manager(SQLITE_DB_PATH)
        cls.manager.execute(
            '''
            CREATE TABLE IF NOT EXISTS `TestModel` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `title` varchar(255) NOT NULL,
                `created_at` DATETIME NOT NULL
            );
            '''
        )

    @classmethod
    def tearDownClass(cls):
        os.remove(SQLITE_DB_PATH)

    def test_insert(self):
        query = TestModel().build_insert_query({'title': 'test_title', 'created_at': datetime.datetime.utcnow()})

        self.assertIsInstance(self.manager.insert(query), int)

    def test_match(self):

        # это заинсертили предыдущим тестом
        self.assertEqual(self.manager.match(TestModel(), {'title': 'test_title'}), 1)

        # такое заматчится не должно
        self.assertIsNone(self.manager.match(TestModel(), {'title': 'none_test'}))

    def test_upsert(self):
        """Проверим что апсерт в пирнципе работает (не падает) и в результате в таблице только одна колонка

        """
        _cond = {'title': 'test_title'}
        _data = dict(_cond, **{'created_at': datetime.datetime.utcnow()})

        self.assertIsInstance(
            self.manager.upsert(TestModel(), _cond, _data),
            int,
        )
        self.assertEqual(int(self.manager.get_one('SELECT COUNT(*) FROM TestModel')[0]), 1)
