# -*- coding: utf-8 -*-
"""
Менеджер для работы с моделями.

Реализует работу с БД и логику работы с моделями: сопоставление, обновление
"""
import sqlite3


class Manager:
    def __init__(self, db_path):
        """
        Args:
            db_path (str): Путь для соединения с БД

        """
        self.db_path = db_path

    connection = None

    def execute(self, query):
        """Выполняет SQL запрос

        Args:
            query (str): строка SQL запроса

        Returns:
            курсор после выполнения запроса

        """
        return self._execute(query)

    def get_one(self, query):
        cur = self.execute(query)

        return cur.fetchone() if cur else None

    def get_all(self, query):
        """
        Зачем я его написал... ведь он не нужен

        Args:
            query:

        Returns:

        """
        cur = self.execute(query)

        return cur.fetchall() if cur else None

    def insert(self, query):
        cur = self._execute(query)
        self.connection.commit()

        return self._returning(cur)

    def match(self, model, conditions):
        """
        Сопоставляется с существующими в БД данными по переданным условиям

        Args:
            model (models.Model): объект модели
            conditions (dict):  словарь условий сопоставления, должен содержать поле таблицы в качестве ключа

        Returns:
            int|None: первичный ключ сопоставленной записи или None

        """
        match_query = model.build_math_query(conditions)
        matched = self.get_one(match_query)

        return int(matched[0]) if matched else None

    def upsert(self, model, conditions, field_values=None):
        """Выполняет текст SQL запроса для вставки или обновления записи.
        (да, новый sqlite с недавнего времени реализовал свой upsert, но мы не ищем легких путей)

        Если уже найдена запимсь со значениями, переданными в conditions — будет выполнен UPDATE, иначе INSERT

        Args:
            model (models.Model): инстанс модели
            conditions (dict): значение полей, по которым производится поиск

            field_values (dict): значения, которые следует записать или обновить.
            Может быть None, тогда этими полями будут conditions (удобно для небольших таблиц, буквально из 1 поля)

        """
        field_values = field_values or conditions
        item_id = self.match(model, conditions)

        if item_id:
            query = model.build_update_by_pk_query(field_values, item_id)
            self.execute(query)
        else:
            query = model.build_insert_query(field_values)
            item_id = self.insert(query)

        return item_id

    def _set_connection(self):
        """Создает и сохраняет в self соединение с БД

        """
        self.connection = sqlite3.connect(self.db_path)

    def _execute(self, query):
        """
        Выполняет запрос, позаботившись о наличии соединения с БД

        Args:
            query (str): sql запрос

        Returns:
            курсор после выполнения запроса

        """
        if not self.connection:
            self._set_connection()

        return self.connection.execute(query)

    def _returning(self, cur):
        """
        Метод возвращает значение, добавленное insert. По умолчанию реализовано для sqlite

        """
        return cur.lastrowid
